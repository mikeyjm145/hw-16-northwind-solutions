﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProductsMaintenace.aspx.cs" Inherits="ProductsMaintenace" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Northwinds Product Maintenance</title>
    <link href="App_Themes/Main.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 2px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <img src="Images/Northwind.jpg" alt="Northwinds logo" />
    <section>
    
        <asp:ListView ID="lvProductsMaintenance" runat="server" DataKeyNames="ProductID" DataSourceID="sqlProductMaintenance" OnItemDeleted="lvProductsMaintenance_ItemDeleted" OnItemUpdated="lvProductsMaintenance_ItemUpdated">
            <AlternatingItemTemplate>
                <table>
                    <tr>
                        <td>ProductID:</td>
                        <td>
                            <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>ProductName:</td>
                        <td>
                            <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>SupplierID:</td>
                        <td>
                            <asp:Label ID="SupplierIDLabel" runat="server" Text='<%# Eval("SupplierID") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>CategoryID:</td>
                        <td>
                            <asp:Label ID="CategoryIDLabel" runat="server" Text='<%# Eval("CategoryID") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>QuantityPerUnit:</td>
                        <td>
                            <asp:Label ID="QuantityPerUnitLabel" runat="server" Text='<%# Eval("QuantityPerUnit") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>UnitPrice:</td>
                        <td>
                            <asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice", "{0:c}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>UnitsInStock:</td>
                        <td>
                            <asp:Label ID="UnitsInStockLabel" runat="server" Text='<%# Eval("UnitsInStock") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>UnitsOnOrder:</td>
                        <td>
                            <asp:Label ID="UnitsOnOrderLabel" runat="server" Text='<%# Eval("UnitsOnOrder") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>ReorderLevel:</td>
                        <td>
                            <asp:Label ID="ReorderLevelLabel" runat="server" Text='<%# Eval("ReorderLevel") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="DiscontinuedCheckBox" runat="server" Checked='<%# Eval("Discontinued") %>' Enabled="False" Text="Discontinued" />
                        </td>
                    </tr>
                        
                </table>
                <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                <br/>
                <br/>
            </AlternatingItemTemplate>
            <EditItemTemplate>
                <table>
                    <tr>
                        <td>ProductID:</td>
                        <td>
                            <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td>ProductName:</td>
                        <td>
                            <asp:TextBox ID="ProductNameTextBox" runat="server" Text='<%# Bind("ProductName") %>' />
                            <asp:RequiredFieldValidator
                                ID="rfvProductName"
                                runat="server"
                                CssClass="error"
                                Display="Dynamic"
                                ErrorMessage="Please enter a value here."
                                ValidationGroup="validate"
                                ControlToValidate="ProductNameTextBox">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>SupplierID:</td>
                        <td>
                            <asp:TextBox ID="SupplierIDTextBox" runat="server" Text='<%# Bind("SupplierID") %>' />
                            <asp:RequiredFieldValidator 
                                ID="rfvSupplierName"
                                runat="server"
                                CssClass="error"
                                Display="Dynamic"
                                ErrorMessage="Please enter a value here."
                                ControlToValidate="SupplierIDTextBox">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>CategoryID:</td>
                        <td>
                            <asp:TextBox ID="CategoryIDTextBox" runat="server" Text='<%# Bind("CategoryID") %>' />
                            <asp:RequiredFieldValidator 
                                ID="rfvCategoryID"
                                runat="server"
                                CssClass="error"
                                Display="Dynamic"
                                ErrorMessage="Please enter a value here."
                                ControlToValidate="CategoryIDTextBox">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>QuantityPerUnit:</td>
                        <td>
                            <asp:TextBox ID="QuantityPerUnitTextBox" runat="server" Text='<%# Bind("QuantityPerUnit") %>' />
                            <asp:RequiredFieldValidator 
                                ID="rfvQuantityPerUnit"
                                runat="server"
                                CssClass="error"
                                Display="Dynamic"
                                ErrorMessage="Please enter a value here."
                                ControlToValidate="QuantityPerUnitTextBox">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>UnitPrice:</td>
                        <td>
                            <asp:TextBox ID="UnitPriceTextBox" runat="server" Text='<%# Bind("UnitPrice") %>' />
                            <asp:RequiredFieldValidator 
                                ID="rfvUnitPrice"
                                runat="server"
                                CssClass="error"
                                Display="Dynamic"
                                ErrorMessage="Please enter a value here."
                                ControlToValidate="UnitPriceTextBox">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>UnitsInStock:</td>
                        <td>
                            <asp:TextBox ID="UnitsInStockTextBox" runat="server" Text='<%# Bind("UnitsInStock") %>' />
                            <asp:RequiredFieldValidator 
                                ID="rfvUnitsInStock"
                                runat="server"
                                CssClass="error"
                                Display="Dynamic"
                                ErrorMessage="Please enter a value here."
                                ControlToValidate="UnitsInStockTextBox">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>UnitsOnOrder:</td>
                        <td>
                            <asp:TextBox ID="UnitsOnOrderTextBox" runat="server" Text='<%# Bind("UnitsOnOrder") %>' />
                            <asp:RequiredFieldValidator 
                                ID="rfvUnitsOnOrder"
                                runat="server"
                                CssClass="error"
                                Display="Dynamic"
                                ErrorMessage="Please enter a value here."
                                ControlToValidate="UnitsOnOrderTextBox">*</asp:RequiredFieldValidator>
                        </td>

                    </tr>
                    <tr>
                        <td>ReorderLevel:</td>
                        <td>
                            <asp:TextBox ID="ReorderLevelTextBox" runat="server" Text='<%# Bind("ReorderLevel") %>' />
                            <asp:RequiredFieldValidator 
                                ID="rfvReorderLevel"
                                runat="server"
                                CssClass="error"
                                Display="Dynamic"
                                ErrorMessage="Please enter a value here."
                                ControlToValidate="ReorderLevelTextBox">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td><asp:CheckBox ID="DiscontinuedCheckBox" runat="server" Checked='<%# Eval("Discontinued") %>' Enabled="false" Text="Discontinued" /></td>
                    </tr>
                </table>
                <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                <br />
                <br />
            </EditItemTemplate>
            <EmptyDataTemplate>
                <span>No data was returned.</span>
            </EmptyDataTemplate>
            <InsertItemTemplate>
                
                <span style="">ProductName:
                <asp:TextBox ID="ProductNameTextBox" runat="server" Text='<%# Bind("ProductName") %>' />
                <br />
                SupplierID:
                <asp:TextBox ID="SupplierIDTextBox" runat="server" Text='<%# Bind("SupplierID") %>' />
                <br />
                CategoryID:
                <asp:TextBox ID="CategoryIDTextBox" runat="server" Text='<%# Bind("CategoryID") %>' />
                <br />
                QuantityPerUnit:
                <asp:TextBox ID="QuantityPerUnitTextBox" runat="server" Text='<%# Bind("QuantityPerUnit") %>' />
                <br />
                UnitPrice:
                <asp:TextBox ID="UnitPriceTextBox" runat="server" Text='<%# Bind("UnitPrice") %>' />
                <br />
                UnitsInStock:
                <asp:TextBox ID="UnitsInStockTextBox" runat="server" Text='<%# Bind("UnitsInStock") %>' />
                <br />
                UnitsOnOrder:
                <asp:TextBox ID="UnitsOnOrderTextBox" runat="server" Text='<%# Bind("UnitsOnOrder") %>' />
                <br />
                ReorderLevel:
                <asp:TextBox ID="ReorderLevelTextBox" runat="server" Text='<%# Bind("ReorderLevel") %>' />
                <br />
                <asp:CheckBox ID="DiscontinuedCheckBox" runat="server" Checked='<%# Bind("Discontinued") %>' Text="Discontinued" />
                <br />
                <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                <br />
                <br />
                </span>
                
            </InsertItemTemplate>
            <ItemTemplate>
                <table>
                    <tr>
                        <td>ProductID:</td>
                        <td>
                            <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>ProductName:</td>
                        <td>
                            <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>SupplierID:</td>
                        <td>
                            <asp:Label ID="SupplierIDLabel" runat="server" Text='<%# Eval("SupplierID") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>CategoryID:</td>
                        <td>
                            <asp:Label ID="CategoryIDLabel" runat="server" Text='<%# Eval("CategoryID") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>QuantityPerUnit:</td>
                        <td>
                            <asp:Label ID="QuantityPerUnitLabel" runat="server" Text='<%# Eval("QuantityPerUnit") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>UnitPrice:</td>
                        <td>
                            <asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice", "{0:c}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>UnitsInStock:</td>
                        <td>
                            <asp:Label ID="UnitsInStockLabel" runat="server" Text='<%# Eval("UnitsInStock") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>UnitsOnOrder:</td>
                        <td>
                            <asp:Label ID="UnitsOnOrderLabel" runat="server" Text='<%# Eval("UnitsOnOrder") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>ReorderLevel:</td>
                        <td>
                            <asp:Label ID="ReorderLevelLabel" runat="server" Text='<%# Eval("ReorderLevel") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="DiscontinuedCheckBox" runat="server" Checked='<%# Eval("Discontinued") %>' Enabled="False" Text="Discontinued" />
                        </td>
                    </tr>
                        
                </table>
                <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                <br/>
                <br/>
            </ItemTemplate>
            <LayoutTemplate>
                <div id="itemPlaceholderContainer" runat="server" style="">
                    <span runat="server" id="itemPlaceholder" />
                </div>
                <div style="">
                    <asp:DataPager ID="DataPager1" runat="server" PageSize="1">
                        <Fields>
                            <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                        </Fields>
                    </asp:DataPager>
                </div>
            </LayoutTemplate>
            <SelectedItemTemplate>
                <span style="">ProductID:
                <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' />
                <br />
                ProductName:
                <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' />
                <br />
                SupplierID:
                <asp:Label ID="SupplierIDLabel" runat="server" Text='<%# Eval("SupplierID") %>' />
                <br />
                CategoryID:
                <asp:Label ID="CategoryIDLabel" runat="server" Text='<%# Eval("CategoryID") %>' />
                <br />
                QuantityPerUnit:
                <asp:Label ID="QuantityPerUnitLabel" runat="server" Text='<%# Eval("QuantityPerUnit") %>' />
                <br />
                UnitPrice:
                <asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice") %>' />
                <br />
                UnitsInStock:
                <asp:Label ID="UnitsInStockLabel" runat="server" Text='<%# Eval("UnitsInStock") %>' />
                <br />
                UnitsOnOrder:
                <asp:Label ID="UnitsOnOrderLabel" runat="server" Text='<%# Eval("UnitsOnOrder") %>' />
                <br />
                ReorderLevel:
                <asp:Label ID="ReorderLevelLabel" runat="server" Text='<%# Eval("ReorderLevel") %>' />
                <br />
                <asp:CheckBox ID="DiscontinuedCheckBox" runat="server" Checked='<%# Eval("Discontinued") %>' Enabled="false" Text="Discontinued" />
                <br />
                <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
<br />
                <br />
                </span>
            </SelectedItemTemplate>
        </asp:ListView>
    
        <asp:SqlDataSource ID="sqlProductMaintenance" runat="server" ConnectionString="<%$ ConnectionStrings:ProductMaintenanceConnectionString %>" ProviderName="<%$ ConnectionStrings:ProductMaintenanceConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [tblProducts] ORDER BY [ProductID]" DeleteCommand="DELETE FROM [tblProducts] WHERE [ProductID] = ?" InsertCommand="INSERT INTO [tblProducts] ([ProductID], [ProductName], [SupplierID], [CategoryID], [QuantityPerUnit], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [ReorderLevel], [Discontinued]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" UpdateCommand="UPDATE [tblProducts] SET [ProductName] = ?, [SupplierID] = ?, [CategoryID] = ?, [QuantityPerUnit] = ?, [UnitPrice] = ?, [UnitsInStock] = ?, [UnitsOnOrder] = ?, [ReorderLevel] = ?, [Discontinued] = ? WHERE [ProductID] = ?">
            <DeleteParameters>
                <asp:Parameter Name="ProductID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="ProductID" Type="Int32" />
                <asp:Parameter Name="ProductName" Type="String" />
                <asp:Parameter Name="SupplierID" Type="Int32" />
                <asp:Parameter Name="CategoryID" Type="Int32" />
                <asp:Parameter Name="QuantityPerUnit" Type="String" />
                <asp:Parameter Name="UnitPrice" Type="Decimal" />
                <asp:Parameter Name="UnitsInStock" Type="Int16" />
                <asp:Parameter Name="UnitsOnOrder" Type="Int16" />
                <asp:Parameter Name="ReorderLevel" Type="Int16" />
                <asp:Parameter Name="Discontinued" Type="Boolean" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="ProductName" Type="String" />
                <asp:Parameter Name="SupplierID" Type="Int32" />
                <asp:Parameter Name="CategoryID" Type="Int32" />
                <asp:Parameter Name="QuantityPerUnit" Type="String" />
                <asp:Parameter Name="UnitPrice" Type="Decimal" />
                <asp:Parameter Name="UnitsInStock" Type="Int16" />
                <asp:Parameter Name="UnitsOnOrder" Type="Int16" />
                <asp:Parameter Name="ReorderLevel" Type="Int16" />
                <asp:Parameter Name="Discontinued" Type="Boolean" />
                <asp:Parameter Name="ProductID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    
        <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    
        <asp:ValidationSummary ID="vsProductsMaintenance" runat="server" CssClass="error" HeaderText="Please correct the following errors:" ValidationGroup="validate" />
    
    </section>
    </form>
</body>
</html>
