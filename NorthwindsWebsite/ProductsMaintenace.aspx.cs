﻿using System;
using System.Web.UI;

/// <summary>
/// Code behind for the ProductMaintenace page
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public partial class ProductsMaintenace : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Handles the ItemUpdated event of the lvProductsMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void lvProductsMaintenance_ItemUpdated(object sender, System.Web.UI.WebControls.ListViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            return;
        }
        e.ExceptionHandled = true;
        e.KeepInEditMode = true;
    }

    /// <summary>
    /// Handles the ItemDeleted event of the lvProductsMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewDeletedEventArgs"/> instance containing the event data.</param>
    protected void lvProductsMaintenance_ItemDeleted(object sender, System.Web.UI.WebControls.ListViewDeletedEventArgs e)
    {
        if (e.Exception == null)
        {
            this.lblError.Text = "";
            return;
        }
        this.lblError.Text = e.Exception.Message;
        e.ExceptionHandled = true;
    }
}